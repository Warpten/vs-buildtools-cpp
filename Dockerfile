# escape=`

# Lecture importante:
#  1. https://learn.microsoft.com/en-us/visualstudio/install/build-tools-container-issues?view=vs-2019
#  2. https://learn.microsoft.com/en-us/visualstudio/install/workload-component-id-vs-build-tools?view=vs-2022#desktop-development-with-c

# Définition de l'image de base
# FROM mcr.microsoft.com/dotnet/framework/sdk:4.8-windowsservercore-ltsc2019
FROM mcr.microsoft.com/windows/servercore:ltsc2022

# Téléchargement des exécutables initialisant l'image
ADD https://aka.ms/vscollect.exe C:\TEMP\collect.exe
ADD https://aka.ms/vs/17/release/channel C:\TEMP\VisualStudio.chman
ADD https://aka.ms/vs/17/release/vs_buildtools.exe C:\TEMP\vs_buildtools.exe
COPY Install.cmd C:\TEMP\
COPY InstallScript.ps1 C:\

SHELL ["powershell", "-command"]
RUN C:\InstallScript.ps1

ENTRYPOINT ["C:\BuildTools\Common7\Tools\VsDevCmd.bat", "&&", "powershell.exe", "-NoLogo", "-ExecutionPolicy", "Bypass"]