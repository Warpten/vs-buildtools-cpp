Write-Host ">> Updating Visual Studio Build Tools ..."
$buildTools = Start-Process "C:\TEMP\Install.cmd" `
    -ArgumentList "C:\TEMP\vs_buildtools.exe", `
        "--wait", "--quiet", "--norestart", "--nocache", `
        "--channelUri", "C:\TEMP\VisualStudio.chman", `
        "--installChannelUri", "C:\TEMP\VisualStudio.chman", `
        "--locale", "en-US", `
        "--installPath", "C:\BuildTools", `
        "--add Microsoft.VisualStudio.Workload.VCTools", `
        "--add Microsoft.VisualStudio.Component.VC.ATL", `
        "--add Microsoft.VisualStudio.Component.VC.ATLMFC", `
        "--includeRecommended", `
        "--remove Microsoft.VisualStudio.Component.VC.ASAN", `
        "--remove Microsoft.VisualStudio.Component.VC.CMake.Project" `
    -NoNewWindow `
    -PassThru `
    -Wait

Write-Host (">> Visual Studio Build tools exited with error code {0}." -f $buildTools.ExitCode)
if ($buildTools.ExitCode -eq 3010) {
    Write-Host ">> [!] Container requires a reboot, which is impossible. Report an issue to Microsoft."
}

exit $buildTools.ExitCode
